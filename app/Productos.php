<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    protected $fillable = [
        'nombre', 'stock', 'pvp', 'descuento', 'precio', 'idGenero', 'idCategoria', 'valoracion', 'descripcion', 'marca', 'distribuidor', 'ruta'
    ];
    public function categoria()
    {
        return $this->belongsTo(Categoria::class, 'idCategoria');
    }
    public function genero()
    {
        return $this->belongsTo(Genero::class, 'idGenero');
    }
}
