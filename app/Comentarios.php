<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentarios extends Model
{
    protected $table = 'comentarios';
    protected $fillable = [
        'idProducto',
        'idUsuario',
        'valoracion',
        'comentario',
    ];
}
