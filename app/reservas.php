<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reservas extends Model
{
    protected $table = 'reservas';
    protected $fillable = [
        'stockReservado', 'stockEntrante', 'idProducto', 'estado', 'fecha_prevista_llegada'
    ];
}
