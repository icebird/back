<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReservaStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !auth()->guard('api')->guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'stockReservado' => [
                'required',
                'integer',
            ],
            'stockEntrante' => [
                'required',
                'integer',
            ],
            'idProducto' => [
                'required',
                'integer',
            ],
            'estado' => [
                'required',
                'string',
                'max:255',
            ],
        ];
    }
}
