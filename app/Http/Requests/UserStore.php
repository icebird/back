<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !auth()->guard('api')->guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user' => [
                'required',
                'string',
                'unique:users,user',
                'max:255',
            ],
            'email' => [
                'required',
                'string',
                'email',
                'unique:users,email',
                'max:255',
            ],
            'password' => [
                'required',
                'string',
                'max:255',
            ],
            /*'rol' => [
                'nullable',
                Rule::in([
                    'user',
                    'admin'
                ]),
            ],*/
        ];
    }
}
