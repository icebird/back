<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductsStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !auth()->guard('api')->guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => [
                'required',
                'string',
                'max:255',
            ],
            'stock' => [
                'required',
                'integer',
            ],
            'pvp' => [
                'required',
                'numeric',
            ],
            'descuento' => [
                'nullable',
                'integer',
            ],
            'precio' => [
                'required',
                'numeric',
            ],
            'idGenero' => [
                'required',
                'integer',
            ],
            'idCategoria' => [
                'required',
                'integer',
            ],
            'descripcion' => [
                'required',
                'string',
            ],
            'marca' => [
                'required',
                'string',
            ],
            'distribuidor' => [
                'required',
                'string',
            ]
        ];
    }
}
