<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Imagenes;

class ImagenesController extends Controller
{
    public function storeImage(Request $request){
        $path = Storage::putFile('products', $request->file('file'));
        $image = Imagenes::create([
            'idProducto' => $request['idProducto'],
            'ruta' => $path,
        ]);
        return 'Creado con exito';
    }
    public function deleteImage(int $idProd) {
        Imagenes::where('idProducto', $idProd)->delete();
    }
}