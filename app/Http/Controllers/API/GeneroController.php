<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoriaStore;
use App\Genero;
use App\Http\Resources\CategoriaCollection;
use App\Http\Resources\CategoriaResource;

class GeneroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new CategoriaCollection(
            Genero::paginate()
        );
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriaStore $request)
    {
        $data = $request->all();
        $categoria = Genero::create($data);
        return $categoria;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $gen = Genero::find($id);
        // return json_encode($gen);
        $cat = Genero::find($id);
        return new CategoriaResource($cat);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'nombre' => 'required',
        ]);

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 422);      
        }
        $genero = Genero::find($id);
        $genero->nombre = $request['nombre'];
        $genero->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $genero = Genero::find($id);
        $genero->delete();
        return "Genero Borrado.";
    }
}
