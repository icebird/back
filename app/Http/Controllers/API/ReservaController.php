<?php

namespace App\Http\Controllers\API;
use App\Http\Resources\ReservaCollection;
use App\Http\Requests\ReservaStore;
use App\reservas;
use App\Http\Resources\ReservaResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ReservaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ReservaCollection(
            reservas::paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReservaStore $request)
    {
        $data = $request->all();
        $reserva = reservas::create($data);
        // return $this->show($reserva);
        return $reserva;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $res = reservas::find($id);
        return new ReservaResource($res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'stockReservado' => 'required',
            'stockEntrante' => 'required',
            'idProducto' => 'required',
            'fecha_prevista_llegada' => 'required',
            'estado' => 'required',
        ]);

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 422);      
        }
        $category = reservas::find($id);
        $category->stockReservado = $request['stockReservado'];
        $category->stockEntrante = $request['stockEntrante'];
        $category->idProducto = $request['idProducto'];
        $category->fecha_prevista_llegada = $request['fecha_prevista_llegada'];
        $category->estado = $request['estado'];
        $category->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = reservas::find($id);
        $res->delete();
        return "Reserva Borrado.";
    }
}