<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pedido;

class PedidosController extends Controller
{
    public function cambiarEstadoEnviado($id) {
        $estado = Pedido::find($id);
        $estado->estado = "Enviado";
        $estado->save();
    }
    
    public function cambiarEstadoEntregado($id) {
        $estado = Pedido::find($id);
        $estado->estado = "Entregado";
        $estado->save();
    }

    public function cambiarEstadoCancelado($id) {
        $estado = Pedido::find($id);
        $estado->estado = "Cancelado";
        $estado->save();
    }
}
