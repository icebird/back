<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\CategoriaStore;
use App\Categoria;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoriaCollection;
use App\Http\Resources\CategoriaResource;
use Validator;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        // $category = Categoria::all();
        // return json_encode($category);
        return new CategoriaCollection(
            Categoria::paginate()
        );
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriaStore $request)
    {
        $data = $request->all();
        $categoria = Categoria::create($data);
        return $categoria;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cat = Categoria::find($id);
        return new CategoriaResource($cat);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'nombre' => 'required',
        ]);

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 422);      
        }
        $category = Categoria::find($id);
        $category->nombre = $request['nombre'];
        $category->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Categoria::find($id);
        $category->delete();
        return "Categoria Borrada.";
    }
}
