<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Productos;
use App\Categoria;
use App\Pedido;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Cliente;
use App\Municipio;
use App\Provincia;
use App\Comunidad;
use App\Comentarios;
use App\User;

use App\Http\Controllers\Controller;
use Laravel\Passport\Bridge\Client;

class ControlController extends Controller
{
    public function ProceedPayment() {
        return (view('pages.payment'));
    }

    public function FiltroProductos(Request $request, $id) {
        if ($request->genes!=null) {
            if ($request->marcs=null) {
                $products = Productos::where('idCategoria', $id)->whereIn('idGenero', $request->genes)->whereIn('marca', $request->marcs)->
                where('pvp', '>=', $request->min)->where('pvp', '<=', $request->max)->get();
            } else {
                $products = Productos::where('idCategoria', $id)->whereIn('idGenero', $request->genes)->where('pvp', '>=', $request->min)->
                where('pvp', '<=', $request->max)->get();
            }
        } else {
            if ($request->marcs!=null) {
                $products = Productos::where('idCategoria', $id)->whereIn('marca', $request->marcs)->where('pvp', '>=', $request->min)->
                where('pvp', '<=', $request->max)->get();
            } else {
                $products = Productos::where('idCategoria', $id)->where('pvp', '>=', $request->min)->where('pvp', '<=', $request->max)->get();
            }
        }
        $category = Categoria::where('id',$id)->get();
        $genders = Productos::select('idGenero', DB::raw('count(*) as filas') )->where('idCategoria', $id)->groupby('idgenero')->get();
        $marcas = Productos::select('marca', DB::raw('count(*) as filas') )->where('idCategoria', $id)->groupby('marca')->get();
        return view('pages.category', compact('products', 'category', 'genders', 'marcas'));
    }

    public function precioTotal($arra) {
        $arr = json_decode($arra);
        $var= 0;
        for ($i=0;$i<count($arr);$i++) {
            $product = Productos::find($arr[$i][0]);
            $var += ($product->pvp-($product->pvp*($product->descuento/100)))*$arr[$i][1];
        }
        return $var;
    }

    public function CreaPedido(Request $request) {
        $arr = json_decode($request->input('datos'));
        $pedido = array(
            'precio_total' => $this->precioTotal($request->input('datos')),
            'productos' => [],
        );
        $arr2 = array();
        for ($i=0;$i<count($arr);$i++) {
            $product = Productos::find($arr[$i][0]);
            $myarr = array('id' => $arr[$i][0], 'nombre' => $product->nombre, 'cantidad' => $arr[$i][1]);
            array_push($pedido['productos'], (object) $myarr);
        }
        $pedid = new Pedido;
        $pedid->pedido = json_encode($pedido);
        $pedid->idUsuario = Auth::user()->id;
        $pedid->estado = 'Pendiente';
        $pedid->save();
        return (count($arr));
    }
    public function makePedido(Request $request) {
        $infocls = Cliente::where('id_usuario', Auth::user()->id)->count();
        if ($infocls!=0) {
            $infocls = Cliente::where('id_usuario', Auth::user()->id)->get();
            foreach ($infocls as $info) {
                $client = Cliente::find($info->id);
            }
        } else {
            $client = new Cliente;
        }
        $client->id_usuario = Auth::user()->id;
        $client->nombre = $request->input('nombre')." ".$request->input('apellido');
        $client->dni = $request->input('dni');
        $pro = array();
        $products = Pedido::where('idUsuario',Auth::user()->id)->get();
        foreach ($products as $product) {
            $var = json_decode($product->pedido);
            foreach($var->productos as $prod) {
                array_push($pro,$prod->id);
            }
        }
        $client->ProductosComprados =json_encode($pro);
        $per = $request->input('status_envio');
        if ($per=="no") {
            $muni = Municipio::find($request->input('id_municipio_fact'));
            $prov = Provincia::find($request->input('id_provincia_fact'));
            $comu = Comunidad::find($request->input('id_comunidad_fact'));
            $client->direccion_envio = $request->input('direccion')." ".$muni->municipio." ".$prov->provincia." ".$comu->comunidad;
            $client->direccion_facturacion = $request->input('direccion')." ".$muni->municipio." ".$prov->provincia." ".$comu->comunidad;
        } 
        if ($per=="si"){
            $muni = Municipio::find($request->input('id_municipio_env'));
            $prov = Provincia::find($request->input('id_provincia_env'));
            $comu = Comunidad::find($request->input('id_comunidad_env'));
            $client->direccion_envio = $request->input('direccion')." ".$muni->municipio." ".$prov->provincia." ".$comu->comunidad;
            $munis = Municipio::find($request->input('id_municipio_fact'));
            $provs = Provincia::find($request->input('id_provincia_fact'));
            $comus = Comunidad::find($request->input('id_comunidad_fact'));
            $client->direccion_facturacion = $request->input('direc_envio')." ".$munis->municipio." ".$provs->provincia." ".$comus->comunidad;
        }
        $client->save();
    }
    public function datosPedido() {
        $pedidos = Pedido::all();
        return $pedidos;
    }
    public function makeComentario(Request $request) {
        $pro = Cliente::where('id_usuario', Auth::user()->id)->get();
        $w = false;
        foreach($pro as $p) {
            $prod = json_decode($p->ProductosComprados);
            for ($i=0;$i<count($prod);$i++) {
                if ($request->opcion==$prod[$i]) {
                    $com = new Comentarios;
                    $com->idProducto = $request->opcion;
                    $com->idUsuario = Auth::user()->id;
                    $com->valoracion = $request->rating;
                    $com->comentario = $request->coment;
                    $com->save();
                    $w = true;
                }
            }
            if ($w==true) {
                break;
            }
        }
        $producto = Productos::find($request->opcion);
        $comentarios = Comentarios::where('idProducto', $request->opcion)->get();
        return view('pages.product', compact('producto', 'comentarios'));
    }
    public function confirmed_mail() {
        $use = User::find(Auth::user()->id);
        return $use->confirmed;
    }
}
