<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\ProductsStore;
use App\Productos;
use App\Categoria;
use App\Genero;
use App\Http\Resources\ProductoResource;
use App\Http\Controllers\Controller;
use DB;
use Session;
use App\Comentarios;

use Validator;

class ProductosController extends Controller
{
    public function precioTotal(Request $request) {
        $arr = json_decode($request->input('array'));
        $var= 0;
        for ($i=0;$i<count($arr);$i++) {
            $product = Productos::find($arr[$i][0]);
            $var += ($product->pvp-($product->pvp*($product->descuento/100)))*$arr[$i][1];
        }
        return $var;
    }

    public function indexProduct($id) {
        $producto = Productos::find($id);
        $comentarios = Comentarios::where('idProducto', $id)->get();
        return view('pages.product', compact('producto', 'comentarios'));
    }
    public function indexCategory($id) {
        $products = Productos::where('idCategoria', $id)->get();
        $category = Categoria::where('id',$id)->get();
        $genders = Productos::select('idGenero', DB::raw('count(*) as filas') )->where('idCategoria', $id)->groupby('idgenero')->get();
        $marcas = Productos::select('marca', DB::raw('count(*) as filas') )->where('idCategoria', $id)->groupby('marca')->get();
        return view('pages.category', compact('products', 'category', 'genders', 'marcas'));
    }

    public function searchProduct(Request $request) {
        $categoria = $request->input('categoria');
        $nombre = $request->input('nombre');
        $resultado = Productos::where('nombre', 'LIKE', '%'.$nombre.'%')
        ->where('idCategoria', $categoria)
        ->get()
        ->pluck('id');
        return $resultado;
    }

    public function filterProducts(Request $request) {
        $categoria = $request->input('categoria');
        $genero = $request->input('genero');
        $marca = $request->input('marca');
        $min = $request->input('min');
        $max = $request->input('max');

        if ($genero == null) {
            $filter = Productos::where('idCategoria', $categoria)
            ->whereIn('marca', $marca)
            ->where('pvp', '>=', $min)
            ->where('pvp', '<=', $max)
            ->get();
        }
        elseif ($marca == null) {
            $filter = Productos::where('idCategoria', $categoria)
            ->whereIn('idGenero', $genero)
            ->where('pvp', '>=', $min)
            ->where('pvp', '<=', $max)
            ->get();
        }
        elseif ($genero == null && $marca == null) {
            $filter = Productos::where('idCategoria', $categoria)
            ->where('pvp', '>=', $min)
            ->where('pvp', '<=', $max)
            ->get();
        }
        else {
            $filter = Productos::where('idCategoria', $categoria)
            ->whereIn('idGenero', $genero)
            ->whereIn('marca', $marca)
            ->where('pvp', '>=', $min)
            ->where('pvp', '<=', $max)
            ->get();
        }

        return $filter;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Productos::with('categoria', 'genero')->get();
        return ProductoResource::collection($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductsStore $request)
    {
        $data = $request->all();
        $producto = Productos::create($data);
        return $producto;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Productos::find($id);
        return new ProductoResource($product);
    }

    public function showAll(Request $request, $id)
    {
        $product = Productos::find($id);
        $product->value= $request->input('valor');
        return $product;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        Productos::find($id)->update($data);

        return $data;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Productos::find($id);
        $product->delete();
        return "Producto Borrado.";
    }
}
