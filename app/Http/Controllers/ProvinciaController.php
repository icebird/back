<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProvinciaController extends Controller
{
    public function listarProvincias($id) {
        $provincias = \App\Provincia::where('comunidad_id', $id)->get();
        return $provincias;
    }
}
