<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MunicipioController extends Controller
{
    public function listarMunicipios($id) {
        $municipios = \App\Municipio::where('provincia_id', $id)->get();
        return $municipios;
    }
}
