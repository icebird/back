<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReservaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'stockReservado' => $this->stockReservado,
            'stockEntrante' => $this->stockEntrante,
            'idProducto' => $this->idProducto,
            'fecha_prevista_llegada' => $this->fecha_prevista_llegada,
            'estado' => $this->estado,
        ];
    }
}
