<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ReservaCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($reserva) {
            return [
                'id' => $reserva->id,
                'stockReservado' => $reserva->stockReservado,
                'stockEntrante' => $reserva->stockEntrante,
                'idProducto' => $reserva->idProducto,
                'estado' => $reserva->estado,
            ];
        });
    }
}
