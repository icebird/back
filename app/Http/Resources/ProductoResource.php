<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id' => $this->id,
            'nombre' => $this->nombre,
            'pvp' => $this->pvp,
            'categoria' => $this->categoria->nombre,
            'genero' => $this->genero->nombre,
            'stock' => $this->stock,
            'descuento' => $this->descuento,
            'precio' => $this->precio,
            'idGenero' => $this->idGenero,
            'idCategoria' => $this->idCategoria,
            'descripcion' => $this->descripcion,
            'marca' => $this->distribuidor,
            'distribuidor' => $this->distribuidor,
        ];
    }
}
