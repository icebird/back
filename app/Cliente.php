<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'clientes';
    protected $fillable = [
        'id_usuario',
        'nombre',
        'user-game',
        'dni',
        'ProductosComprados',
        'direccion_envio',
        'direccion_facturacion',
    ];
}
