<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.inicio');
});

Route::get('/category/{id}', 'API\ProductosController@indexCategory');

Route::get('/product/{id}', 'API\ProductosController@indexProduct');
Auth::routes();

Route::get('/search', 'API\ProductosController@searchProduct');

Route::get('/filter', 'API\ProductosController@filterProducts');

Route::get('/provincias/{id}', 'ProvinciaController@listarProvincias');

Route::get('/municipios/{id}', 'MunicipioController@listarMunicipios');

Route::get('/cart', function() {
    return view('pages.cart');
});

Route::get('/profile', function() {
   return view('pages.profile');
})->middleware('auth');

Route::get('/register/verify/{code}', 'GuestController@verify');
//Route::get('/myaccount', 'API\AuthController@MyAccount');
Route::get('productoCart/{id}', 'API\ProductosController@show');
Route::get('productoCarta/{id}', 'API\ProductosController@showAll');
Route::get('PrecioPedido', 'API\ProductosController@precioTotal');
Route::get('payment', 'API\ControlController@ProceedPayment')->middleware('auth');
Route::get('ProductosFiltros/{id}', 'API\ControlController@FiltroProductos');
Route::get('terminos', function () {
    return view("pages.terminos");
});
Route::get('prueba','API\ControlController@testing');
Route::get('GuardarPedido','API\ControlController@CreaPedido');
Route::get('/hacerPedido', 'API\ControlController@makePedido');
Route::get('/comentarios', 'API\ControlController@makeComentario');
Route::get('confirmed', 'API\ControlController@confirmed_mail');
