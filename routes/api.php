<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('API')->group(function() {
    Route::middleware('auth:api')->group(function() {
        Route::middleware('is_admin')->group(function () {
            Route::post('media', 'ImagenesController@storeImage');
            Route::resource('users', 'UserController');
            Route::resource('products', 'ProductosController');
            Route::get('rol', 'AuthController@rolAdmin');
            Route::resource('category', 'CategoriasController');
            Route::resource('gender', 'GeneroController');
            Route::resource('reserve', 'ReservaController');
            Route::get('orders', 'ControlController@datosPedido');
        });
    });
    Route::put('send/{id}', 'PedidosController@cambiarEstadoEnviado');
    Route::put('delivered/{id}', 'PedidosController@cambiarEstadoEntregado');
    Route::put('cancelled/{id}', 'PedidosController@cambiarEstadoCancelado');
    Route::post('register', 'AuthController@register');
    Route::get('logout', 'AuthController@logout');
    Route::get('sesiones', 'ControlController@sesiones');
});