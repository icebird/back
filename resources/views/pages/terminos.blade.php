@extends('layouts.default') @section('content')
<div class="container content">
    <h1>Condiciones generales</h1>
    <p>
        La realización de un pedido en www.icebird.cat supone la aceptación por parte del cliente de las condiciones aquí reflejadas.
    </p>
    <h2>Datos de facturación</h2>

    <p>
        Los datos de facturación son los indicados en su cuenta de cliente y no se podrá modificar posteriormente en cumplimiento
        de la normativa vigente (Real Decreto 1619/2012, de 30 de noviembre, por el que se aprueba el Reglamento por el que
        se regulan las obligaciones de facturación). La factura se emitirá a nombre de la persona física o empresa que realiza
        el pedido, por lo que el cliente debe asegurarse de realizar el pedido con la cuenta de cliente correcta. No serán
        posibles cambios posteriores. Si desea que la factura vaya a otro nombre o empresa diferente, tendrá que estar registrado
        como cliente con los datos de facturación deseados y hacer el pedido bajo esta cuenta.
    </p>

    <p>
        Al realizar la compra acepta recibir la factura en su correo electrónico. Igualmente quedará archivada en su zona de
        cliente y podrá consultarla, descargarla e imprimirla las veces que necesite. No obstante, si lo desea, tiene derecho
        a recibir la factura en papel. Puede solicitarla a través de nuestro sistema de tickets de soporte y se la haremos
        llegar por correo ordinario.
    </p>

    <h2>Orden de compra</h2>

    <p>
        El documento electrónico en el que se finaliza el contrato quedará archivado en la administración de la empresa y puede
        ser accesible mediante acceso privado con los datos de registro del cliente.
    </p>

    <h2>Modificación de datos</h2>

    <p>
        En esta misma pantalla puede comprobar los datos introducidos, en caso de no ser correctos, puede volver atrás y modificarlos.
        También puede solicitar cualquier cambio en sus datos a través de nuestro formulario de contacto.
    </p>

    <h2>Envíos</h2>

    <p>
        Los plazos de entrega oscilan entre las 24 y las 72 horas a elección del cliente. No podemos garantizar estos plazos,
        si bien intentamos que las empresas de transporte los cumplan siempre que sea posible. El envío dependerá de la disponibilidad
        de cada producto, la cual se encuentra indicada en todos los artículos ofertados. En los pedidos que incluyan varios
        productos se hará un único envío, cuando todos ellos se encuentren disponibles. Se considerará entregado un pedido
        cuando sea firmado el recibo de entrega por parte del cliente. A partir de ahí el cliente dispondrá de 72 horas para
        comprobar la integridad de todos los componentes del pedido y para comprobar que se incluye todo lo que debe en los
        productos incluidos. Pasadas estas 72 horas se dará por aceptado el envío y no se aceptarán reclamaciones por desperfectos
        o fallos con el envío. En caso de recibir un producto dañado por el transporte se debe contactar con nosotros mediante
        nuestro formulario de contacto haciendo click aquí indicando el número de pedido y el problema que presenta. Así
        podremos reclamar a la empresa de transporte. Una vez recibida la incidencia le será tramitado un nuevo envío si
        así lo requiere su caso.
    </p>

    <p>
        También es conveniente dejar constancia a la propia empresa de transporte:
    </p>

    <p>
        <ul>
            <li>Tourline Express: 902 34 33 22</li>
            <li>Seur: 902 10 10 10</li>
            <li>Correos: 902 19 71 97</li>
            <li>Zeleris: 902 16 26 46</li>
        </ul>
    </p>
    <h2>Devoluciones</h2>

    <p>Conforme a la legislación vigente, se puede proceder a la devolución de productos, por el motivo que sea, en un plazo
        de 30 días naturales desde la recepción de la mercancía por el cliente. Para ello se deben cumplir las condiciones
        expuestas en esta página de condiciones. Lo podrá hacer cumplimentando el documento de desistimiento accediendo con
        su usuario y contraseña.</p>

    <p>Condiciones de devoluciones para clientes particulares y empresas</p>

    <p>No se aceptarán devoluciones de los siguientes productos, tal y como establece el Real Decreto Legislativo 1/2007, de
        16 de noviembre, por el que se aprueba el texto refundido de la Ley General para la Defensa de los Consumidores y
        Usuarios y otras leyes complementarias:</p>
    <ul>
        <li>La prestación de servicios, una vez que el servicio haya sido completamente ejecutado, cuando la ejecución haya comenzado,
            con previo consentimiento expreso del consumidor y usuario y con el reconocimiento por su parte de que es consciente
            de que, una vez que el contrato haya sido completamente ejecutado por el empresario, habrá perdido su derecho
            de desistimiento.</li>
        <li>El suministro de bienes o la prestación de servicios cuyo precio dependa de fluctuaciones del mercado financiero
            que el empresario no pueda controlar y que puedan producirse durante el periodo de desistimiento.</li>
        <li>El suministro de bienes confeccionados conforme a las especificaciones del consumidor y usuario o claramente personalizados.</li>
        <li>El suministro de bienes que puedan deteriorarse o caducar con rapidez.</li>
        <li>El suministro de bienes precintados que no sean aptos para ser devueltos por razones de protección de la salud o
            de higiene y que hayan sido desprecintados tras la entrega.</li>
        <li>El suministro de bienes que después de su entrega y teniendo en cuenta su naturaleza se hayan mezclado de forma indisociable
            con otros bienes.</li>
        <li>El suministro de grabaciones sonoras o de vídeo precintadas o de programas informáticos precintados que hayan sido
            desprecintados por el consumidor y usuario después de la entrega.</li>
        <li>El suministro de contenido digital que no se preste en un soporte material cuando la ejecución haya comenzado con
            el previo consentimiento expreso del consumidor y usuario con el conocimiento por su parte de que en consecuencia
            pierde su derecho de desistimiento.</li>
        <li>Toda mercancía debe ser devuelta en su embalaje original con todos los accesorios que contenga, en perfecto estado
            y protegida, evitando pegatinas, precintos o cintas adhesivas directamente sobre la superficie o embalaje del
            artículo. En caso contrario icebird se reserva el derecho de rechazar la devolución.</li>
        <li>Una vez rellenado y enviado el formulario de devolución recibirá las instrucciones para que nos lo haga llegar a
            nuestras instalaciones en su correo electrónico. Deberá enviar los bienes sin ninguna demora, en un plazo máximo
            de 14 días desde que nos comunique su deseo de ejercer el derecho.</li>
    </ul>
    <p>
        Los gastos de transporte originados por la devolución correrán a su cargo. Usted es libre de elegir y buscar la agencia que
        más se adapte a sus necesidades o le ofrezca las tarifas más competitivas. No obstante puede realizar un cálculo del
        coste que puede acarrearle la devolución aquí: ASM Tourline Zeleris Correos Express Una vez recibida la mercancía y comprobada
        que está en perfectas condiciones, se tramitará la devolución del importe. Le devolveremos el pago recibido de usted,
        incluido el gasto de entrega con la excepción de los gastos adicionales resultantes de la elección por su parte de una
        modalidad de entrega diferente a la modalidad menos costosa de entrega ordinaria que ofrezcamos. Le realizaremos el abono
        en un plazo máximo de 14 días naturales desde que ejerza su derecho de desistimiento. Hasta que no hayamos recibido los
        bienes podremos retener el reembolso.
    </p>

    <h2>Condiciones de devoluciones para distribuidores</h2>
    <p>
        Únicamente se aceptarán devoluciones de material sin desprecintar y en perfecto estado, durante los 14 días naturales posteriores
        a su recepción. Tales devoluciones serán tramitadas como devolución comercial, ya que no hay ninguna ley que regule los
        derechos de devolución entre empresas y dichas tramitaciones están reguladas según las condiciones de ICEBIRD. No se
        aceptarán devoluciones de productos en blíster o termosellado, y resto de productos que presenten un precinto de fábrica
        de seguridad roto; como por ejemplo, placas base con cables desprecintados, monitores, impresoras, tarjetas de memoria,
        etc. Dichas condiciones no anulan el derecho de garantía o cambio de productos defectuosos. ICEBIRD se reserva el derecho
        a denegar la devolución en caso de detectarse cualquier anomalía en el producto devuelto.
    </p>
    <h2>Garantías</h2>

    <p>
        Toda la información relativa a garantías de los productos la encuentra en la sección de garantías
    </p>

    <h2>Cancelaciones de pedidos</h2>

    <p>
        Aquellas cancelaciones de pedidos que impliquen una devolución al cliente y que sean por transferencia bancaria tendrán un
        plazo máximo de 30 días por trámites administrativos, si bien intentamos que el plazo no sea superior a 7 días.
    </p>

    <h2>Consultas y reclamaciones</h2>

    <p>
        Puede efectuar todas sus consultas o reclamaciones a través de nuestro sistema de ticket o a la dirección ICEBIRD . Avda
        de Europa, parcelas 2-5 y 2-6 Polígono Industrial Las Salinas C.P. 30840 Alhama de Murcia (Barcelona).
    </p>
</div>
@stop