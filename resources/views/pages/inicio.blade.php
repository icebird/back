@extends('layouts.default')
@section('content')
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- shop -->
                <div class="col-md-4 col-xs-6">
                    <div class="shop">
                        <div class="shop-img">
                            <img src="https://www.programas-gratis.net/blog/wp-content/uploads/2013/07/los-mejores-juegos-2013.jpg" height="240px" width="360px" alt="">
                        </div>
                        <div class="shop-body">
                            <h3>Los mejores<br>Videojuegos</h3>
                            <a href="#" class="cta-btn">Ver ahora! <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- /shop -->

                <!-- shop -->
                <div class="col-md-4 col-xs-6">
                    <div class="shop">
                        <div class="shop-img">
                            <img src="http://static.alfabetajuega.com/abj_public_files/multimedia/imagenes/201503/102050.alfabetajuega-figuras-adorables-league-o-legends-030315-1.jpg" width="360px" alt="" height="240px">
                        </div>
                        <div class="shop-body">
                            <h3>El mejor<br>Merchandising</h3>
                            <a href="#" class="cta-btn">Ver ahora! <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- /shop -->

                <!-- shop -->
                <div class="col-md-4 col-xs-6">
                    <div class="shop">
                        <div class="shop-img">
                            <img src="http://truequeshop.es/c/75-large_default/accesorios-de-consolas.jpg" alt="" height="240px" width="360px">
                        </div>
                        <div class="shop-body">
                            <h3>Accesorios<br>Imprescindibles</h3>
                            <a href="#" class="cta-btn">Ver ahora! <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- /shop -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
@stop