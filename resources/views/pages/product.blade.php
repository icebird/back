@extends('layouts.default')
@section('content')
<!-- SECTION -->
<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- Product main img -->
            <div class="col-md-5 col-md-push-2">
                <div id="product-main-img">
                    <div class="product-preview">
                        <img src="https://thumb.pccomponentes.com/w-220-220/articles/13/137320/1373200.jpg" alt="">
                    </div>
                    <div class="product-preview">
                        <img src="https://thumb.pccomponentes.com/w-220-220/articles/13/137320/1373200.jpg" alt="">
                    </div>
                    <div class="product-preview">
                        <img src="https://thumb.pccomponentes.com/w-220-220/articles/13/137320/1373200.jpg" alt="">
                    </div>
                    <div class="product-preview">
                        <img src="https://thumb.pccomponentes.com/w-220-220/articles/13/137320/1373200.jpg" alt="">
                    </div>
                </div>
            </div>
            <!-- /Product main img -->

            <!-- Product thumb imgs -->
            <div class="col-md-2 col-md-pull-5">
                <div id="product-imgs">
                    <div class="product-preview">
                        <img src="https://thumb.pccomponentes.com/w-220-220/articles/13/137320/1373200.jpg" alt="">
                    </div>

                    <div class="product-preview">
                        <img src="https://thumb.pccomponentes.com/w-220-220/articles/13/137320/1373200.jpg" alt="">
                    </div>

                    <div class="product-preview">
                        <img src="https://thumb.pccomponentes.com/w-220-220/articles/13/137320/1373200.jpg" alt="">
                    </div>

                    <div class="product-preview">
                        <img src="https://thumb.pccomponentes.com/w-220-220/articles/13/137320/1373200.jpg" alt="">
                    </div>
                </div>
            </div>
            <!-- /Product thumb imgs -->

            <!-- Product details -->
            <div class="col-md-5">
                <div class="product-details">
                    <h2 class="product-name">{{$producto->nombre}}</h2>
                    <div>
                        <div class="product-rating">
                            @if($producto->valoracion == null || $producto->valoracion == 0)
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                            @endif
                        </div>
                        <a class="review-link" data-toggle="tab" href="#tab2">{{ \App\Comentarios::where('idProducto', $producto->id)->count()}} Comentario(s) | Añade tu comentario</a>
                    </div>

                    <div>
                        @if($producto->descuento == 0)
                            <h3 class="product-price">{{$producto->pvp}}€</h3>
                        @else
                            <h3 class="product-price">{{$producto->pvp-($producto->pvp*$producto->descuento/100)}}€ <del class="product-old-price">{{$producto->pvp}}€</del></h3>
                        @endif

                        @if($producto->stock > 0)
                            <span class="product-available">
                                Disponible
                            </span>
                        @else
                            <span class="product-not-available">
                                No Disponible
                            </span>
                        @endif
                    </div>

                    <div class="add-to-cart">
                        <div class="qty-label">
                            Cantidad:
                            <div class="input-number">
                                <input id="quantity" value="1" type="number">
                                <span class="qty-up">+</span>
                                <span class="qty-down">-</span>
                            </div>
                        </div>
                        <button class="add-to-cart-btn" onclick="anyadirCarrito({{ $producto->id }}, -5)"><i class="fa fa-shopping-cart"></i> Añadir al carrito</button>
                    </div>

                    <ul class="product-links">
                        <li>Categoría:</li>
                        <li><a class="gender" href="/category/{{$producto->idCategoria}}">{{ \App\Categoria::where('id', $producto->idCategoria)->get()->pluck('nombre') }}</a></li>
                        <li>Género:</li>
                        <li class="gender">{{ \App\Genero::where('id', $producto->idGenero)->get()->pluck('nombre') }}</li>
                    </ul>
                </div>
            </div>
            <!-- /Product details -->

            <!-- Product tab -->
            <div class="col-md-12">
                <div id="product-tab">
                    <!-- product tab nav -->
                    <ul class="tab-nav">
                        <li id="first-tab" class="active"><a data-toggle="tab" href="#tab1">Descripción</a></li>
                        <li id="second-tab"><a data-toggle="tab" href="#tab2">Comentarios ({{ \App\Comentarios::where('idProducto', $producto->id)->count()}})</a></li>
                    </ul>
                    <!-- /product tab nav -->

                    <!-- product tab content -->
                    <div class="tab-content">
                        <!-- tab1  -->
                        <div id="tab1" class="tab-pane fade in active">
                            <div class="row">
                                <div class="col-md-12">
                                <p>{{$producto->descripcion}}</p>
                                </div>
                            </div>
                        </div>
                        <!-- /tab1  -->

                        <!-- tab2  -->
                        <div id="tab2" class="tab-pane fade in">
                            <div class="row">
                                <!-- Reviews -->
                                <div class="col-md-6">
                                    <div id="reviews">
                                        <ul class="reviews">
                                            @foreach($comentarios as $comentario)
                                                <li>
                                                    <div class="review-heading">
                                                        <h5 class="name">
                                                            {{\App\User::find($comentario->idUsuario)->user}}
                                                        </h5>
                                                        <p class="date">{{$comentario->created_at}}</p>
                                                        <div class="review-rating">
                                                            @for($i=0;$i<5;$i++)
                                                                @if($i<$comentario->valoracion)
                                                                    <i class="fa fa-star"></i>
                                                                @else
                                                                    <i class="fa fa-star-o empty"></i>
                                                                @endif

                                                            @endfor
                                                        </div>
                                                    </div>
                                                    <div class="review-body">
                                                        <p>{{$comentario->comentario}}</p>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <!-- /Reviews -->

                                <!-- Review Form -->
                                <div class="col-md-3">
                                    <div id="review-form">
                                        <form class="review-form" action="/comentarios">
                                            <textarea name="coment" class="input" placeholder="Mensaje"></textarea>
                                            <div class="input-rating">
                                                <span>Puntúa: </span>
                                                <div class="stars">
                                                    <input id="star5" name="rating" value="5" type="radio"><label for="star5" checked></label>
                                                    <input id="star4" name="rating" value="4" type="radio"><label for="star4"></label>
                                                    <input id="star3" name="rating" value="3" type="radio"><label for="star3"></label>
                                                    <input id="star2" name="rating" value="2" type="radio"><label for="star2"></label>
                                                    <input id="star1" name="rating" value="1" type="radio"><label for="star1"></label>
                                                </div>
                                            </div>
                                            <input type="hidden" name="opcion" value="{{$producto->id}}">
                                            <button type="submit" class="primary-btn">Enviar</button>
                                        </form>
                                    </div>
                                </div>
                                <!-- /Review Form -->
                            </div>
                        </div>
                        <!-- /tab2  -->
                    </div>
                    <!-- /product tab content  -->
                </div>
            </div>
            <!-- /product tab -->
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
<!-- /SECTION -->

<!-- Section -->
<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            @if( \App\Productos::where('idCategoria', $producto->idCategoria)->where('id', '!=', $producto->id)->count() > 0 )
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h3 class="title">Productos Relacionados</h3>
                    </div>
                </div>

                <!-- product -->
                @foreach( \App\Productos::where('idCategoria', $producto->idCategoria)->where('id', '!=', $producto->id)->get() as $iguales)
                    <div class="col-md-3 col-xs-6">
                        <div class="product">
                            <div class="product-img">
                                <img src="https://thumb.pccomponentes.com/w-220-220/articles/13/137320/1373200.jpg" alt="">
                                <div class="product-label">
                                    @if($iguales->descuento > 0 || $iguales->descuento != 0)
                                        <span class="sale">{{$iguales->descuento}}%</span>
                                    @else
                                    @endif
                                </div>
                            </div>
                            <div class="product-body">
                            <p class="product-category gender">{{ \App\Categoria::where('id', $iguales->idCategoria)->get()->pluck('nombre') }}</p>
                            <h3 class="product-name"><a href="/product/{{$iguales->id}}">{{$iguales->nombre}}</a></h3>
                            @if($iguales->descuento == 0)
                                <h4 class="product-price">{{$iguales->pvp}}€</h4>
                            @else
                                <h4 class="product-price">{{$iguales->pvp-($iguales->pvp*$iguales->descuento/100)}}€ <del class="product-old-price">{{$iguales->pvp}}€</del></h4>
                            @endif
                                <div class="product-rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <div class="product-btns">
                                    <button class="quick-view" onclick="verProducto({{ $iguales->id }})"><i class="fa fa-eye"></i><span class="tooltipp">Ver producto</span></button>
                                </div>
                            </div>
                            <div class="add-to-cart">
                                <button class="add-to-cart-btn" onclick="anyadirCarrito({{ $iguales->id }}, 1)"><i class="fa fa-shopping-cart"></i> Añadir al carrito</button>
                            </div>
                        </div>
                    </div>
                <!-- /product -->
                @endforeach
                <div class="clearfix visible-sm visible-xs"></div>
            </div>
            @else
                
            @endif
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
<!-- /Section -->
@stop