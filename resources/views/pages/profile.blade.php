@extends('layouts.default')
@section('content')
<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">{{ Auth::user()->user }}</h3>
    </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="https://i2.wp.com/drogaspoliticacultura.net/wp-content/uploads/2017/09/placeholder-user.jpg" class="img-circle img-responsive"> </div>
                <div class=" col-md-9 col-lg-9 ">
                    <table class="table table-user-information">
                        <tbody>
                            <tr>
                                <td>Nombre:</td>
                                <td class="gender">{{ \App\Cliente::where('id_usuario', Auth::user()->id)->get()->pluck('nombre') }}</td>
                            </tr>
                            <tr>
                                <td>DNI:</td>
                                <td class="gender">{{ \App\Cliente::where('id_usuario', Auth::user()->id)->get()->pluck('dni') }}</td>
                            </tr>
                            <tr>
                                <td>Dirección de facturación:</td>
                                <td class="gender">{{ \App\Cliente::where('id_usuario', Auth::user()->id)->get()->pluck('direccion_facturacion') }}</td>
                            </tr>
                            @if (\App\Cliente::where('id_usuario', Auth::user()->id)->get()->pluck('direccion_envio') == '[""]' )
                                <tr>
                                    <td>Dirección de envío:</td>
                                    <td class="gender">{{ \App\Cliente::where('id_usuario', Auth::user()->id)->get()->pluck('direccion_facturacion') }}</td>
                                </tr>
                            @else
                                <tr>
                                    <td>Dirección de envío:</td>
                                    <td class="gender">{{ \App\Cliente::where('id_usuario', Auth::user()->id)->get()->pluck('direccion_envio') }}</td>
                                </tr>
                            @endif
                            <tr>
                                <td>Email:</td>
                                <td><input class="input" value={{ Auth::user()->email }} type="email" name="email" placeholder="Email"></td>
                            </tr>
                            <tr>
                                <td>Pedido(s):</td>
                                <td>
                                    <table class="table">
                                        <tr><th>Producto</th><th>Estado</th><th>Cantidad</th></tr>
                                    @foreach( \App\Pedido::where('idUsuario', Auth::user()->id)->get() as $pedido )
                                        @for ($i=0; $i<count(json_decode($pedido->pedido)->productos); $i++)
                                            <tr><td>{{ json_decode($pedido->pedido)->productos[$i]->nombre }}</td>
                                                @if (json_decode($pedido)->estado == "Pendiente")
                                                    <td><i title="Pendiente" class="fa fa-clock-o" aria-hidden="true"></i></td>
                                                @elseif (json_decode($pedido)->estado == "Enviado")
                                                    <td><i title="Enviado" class="fa fa-truck" aria-hidden="true"></i></td>
                                                @elseif (json_decode($pedido)->estado == "Entregado")
                                                    <td><i title="Entregado" class="fa fa-check" aria-hidden="true"></i></td>
                                                @elseif (json_decode($pedido)->estado == "Cancelado")
                                                    <td><i title="Cancelado" class="fa fa-times" aria-hidden="true"></i></td>
                                                @endif
                                                <td>{{ json_decode($pedido->pedido)->productos[$i]->cantidad }}</td>
                                            </tr>
                                        @endfor
                                    @endforeach
                                    </table>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel-footer">
                <span class="pull-right">
                    <a data-original-title="Guardar" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="fa fa-floppy-o"></i></a>
                </span>
            </div>
        </div>
    </div>
</div>
@stop