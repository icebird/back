@extends('layouts.default')
@section('content')
<section class="cart bgwhite p-t-70 p-b-100">
    <div class="container">
        <!-- Cart item -->
        <div class="container-table-cart pos-relative">
            <div class="wrap-table-shopping-cart bgwhite">
                <table id="cart-page" class="table-shopping-cart">

                </table>
            </div>
        </div>
        <!-- Total -->
        <div class="bo9 w-size18 p-l-40 p-r-40 p-t-30 p-b-38 m-t-30 m-r-0 m-l-auto p-lr-15-sm m-b-26">
            <h5 class="m-text20">
                Total del carrito
            </h5>

            <!--  -->
            <div class="row">
                <span class="col-md-6 m-text22">
                    Total:
                </span>

                <span id="precio_final" class="col-md-6 m-text21">
                    
                </span>
            </div>

            <div class="size15 m-t-26">
                <a href='/payment'><button class="ship-btn sizefull">
                    Proceder al pago
                </button></a>
            </div>
                <!-- Button -->
        </div>
    </div>
</section>
@stop