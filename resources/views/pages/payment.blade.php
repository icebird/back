@extends('layouts.default')
@section('content')
<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <div class="col-md-7">
                <!-- Billing Details -->
                <div class="billing-details">
                    <div class="section-title">
                        <h3 class="title">Dirección de facturación</h3>
                    </div>
                     <div class="form-group">
                        <input class="form-control" type="text" id="nombre" placeholder="Nombre" required>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" id="apellido" placeholder="Apellidos" required>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="dni" id="dni" placeholder="DNI" required>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" id="direccion" placeholder="Dirección" required>
                    </div>
                    <div class="form-group">
                        <select name="1" id="comunidad" class="form-control 1"  placeholder="Comunidad" required>
                            <option value="0">--- SELECCIONAR COMUNIDAD ---</option>
                            @foreach( \App\Comunidad::all() as $comunidad)
                                <option value={{$comunidad->id}}>{{$comunidad->comunidad}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="2" id="provincia" class="form-control 2"  placeholder="Provincia" required>
                            <option value="0">--- SELECCIONAR PROVINCIA ---</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <select name="3" id="municipio" class="form-control 3"  placeholder="Municipio" required>
                            <option value="0">--- SELECCIONAR MUNICIPIO ---</option>
                        </select>
                    </div>
                    
                </div>
                <!-- /Billing Details -->

                <!-- Shiping Details -->
                <div class="shiping-details">
                    <div class="section-title">
                        <h3 class="title">Dirección de envio</h3>
                    </div>
                    <div class="input-checkbox">
                        <input name="direc_envio" type="checkbox" id="shiping-address">
                        <label for="shiping-address">
                            <span></span>
                            Enviar a una dirección diferente
                        </label>
                        <div class="caption">
                            <div class="form-group">
                                <input class="input" type="text" id="address_envio" placeholder="Dirección">
                            </div>
                            <div class="form-group">
                                <select name="4" id="comunidad2" class="form-control" id="comunidad_envio" placeholder="Comunidad">
                                    <option id='4' value="0">--- SELECCIONAR COMUNIDAD ---</option>
                                    @foreach( \App\Comunidad::all() as $comunidad)
                                        <option value={{$comunidad->id}}>{{$comunidad->comunidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <select name="5" id="provincia2" class="form-control" id="provincia_envio" placeholder="Provincia">
                                    <option id='5' value="0">--- SELECCIONAR PROVINCIA ---</option>
                                </select>
                            </div>
        
                            <div class="form-group">
                                <select name="6" id="municipio2" class="form-control" id="municipio_envio" placeholder="Municipio">
                                    <option id='6' value="0">--- SELECCIONAR MUNICIPIO ---</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Shiping Details -->

                <!-- Order notes -->
                <div class="order-notes">
                    <textarea name="" class="input" placeholder="Notas adicionales"></textarea>
                </div>
                <!-- /Order notes -->
            </div>

            <!-- Order Details -->
            <div class="col-md-5 order-details">
                <div class="section-title text-center">
                    <h3 class="title">Tu pedido</h3>
                </div>
                <div class="order-summary">
                    <div class="order-products">
                    </div>
                    <div class="order-col">
                        <div>Envío</div>
                        <div><strong>GRATIS</strong></div>
                    </div>
                    <div class="order-col">
                        <div><strong>TOTAL</strong></div>
                        <div><strong id="price-product-final" class="order-total">0€</strong></div>
                    </div>
                </div>
                <div class="payment-method">
                    <div class="input-radio">
                        <input type="radio" name="payment" id="payment-1">
                        <label for="payment-1">
                            <span></span>
                            Transferéncia bancaria
                        </label>
                        <div class="caption">
                            <p>Deberás realizar una transferencia o un ingreso en ventanilla, en las entidades MiLibro o LunaDePluton, por el importe total del pedido. Tu pedido será preparado a partir de que recibamos el pago, en un plazo que puede oscilar entre 24 y 72 horas, dependiendo de la entidad bancaria. Podrás ver los datos para realizar la transferencia al finalizar tu pedido y también te los enviaremos a tu correo electrónico, junto al resumen de tu compra.
                                Importante: Si a los 4 días, desde que realizaste tu pedido, no hemos recibido la transferencia, este será eliminado. Por lo tanto, te recomendamos realizar la transferencia en las próximas 24 horas. </p>
                        </div>
                    </div>
                    <div class="input-radio">
                        <input type="radio" name="payment" id="payment-2" checked="checked">
                        <label for="payment-2">
                            <span></span>
                            Paypal
                        </label>
                        <div class="caption">
                            <p>Aqui hubiera ido un pago con paypal xd</p>
                        </div>
                    </div>
                </div>
                <div class="input-checkbox">
                    <input type="checkbox" id="terms">
                    <label for="terms">
                        <span></span>
                        He leído y acepto los <a href="/terminos">Términos y Condiciones</a>
                    </label>
                </div>
                <p>&nbsp</p>
                <button onclick="realizarCompra()" class="primary-btn">Realizar pedido</button>
            </div>
            <!-- /Order Details -->
        </form>
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
@stop