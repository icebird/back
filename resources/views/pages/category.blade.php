@extends('layouts.default')
@section('content')
    <!-- BREADCRUMB -->
    <div id="breadcrumb" class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb-tree">
                        <li><a href="/">Inicio</a></li>
                        <li><a href="#">Videojuegos</a></li>
                        @foreach($category as $item)
                        <li id='{{ $item->id }}' class="active category-id">{{ $item->nombre }}</li>
                    </ul>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /BREADCRUMB -->

    <!-- SECTION -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- ASIDE -->
                <div id="aside" class="col-md-3">
                    <form method="GET" action="/ProductosFiltros/{{$item->id}}">
                    @endforeach
                    <!-- aside Widget -->
                    <div class="aside">
                        <h3 class="aside-title">Generos</h3>
                        <div class="checkbox-filter">
                            @foreach($genders as $genero)
                                <div class="input-checkbox">
                                    <input name="genes[]" class="gender-check" value={{$genero->idGenero}} type="checkbox" id="category-{{$genero->idGenero}}">
                                    <label for="category-{{$genero->idGenero}}">
                                        <span></span>
                                        <strong class="gender" style="font-weight: normal">{{App\Genero::where('id', $genero->idGenero)->pluck('nombre')}}</strong>
                                        <small>
                                             ({{$genero->filas}})
                                        </small>
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- /aside Widget -->

                    <!-- aside Widget -->
                    <div class="aside">
                        <h3 class="aside-title">Precio</h3>
                        <div class="price-filter">
                            <div id="price-slider"></div>
                            <div class="input-number price-min">
                                <input name="min" id="price-min" type="number">
                                <span class="qty-up">+</span>
                                <span class="qty-down">-</span>
                            </div>
                            <span>-</span>
                            <div class="input-number price-max">
                                <input name="max" id="price-max" type="number">
                                <span class="qty-up">+</span>
                                <span class="qty-down">-</span>
                            </div>
                        </div>
                    </div>
                    <!-- /aside Widget -->

                    <!-- aside Widget -->
                    <div class="aside">
                        <h3 class="aside-title">Marca</h3>
                        <div class="checkbox-filter">
                            @foreach($marcas as $index => $marca)
                                <div class="input-checkbox">
                                <input name="marcs[]" class="brand-check" value={{$marca['marca']}} type="checkbox" id="brand-{{$index}}">
                                    <label for="brand-{{$index}}">
                                        <span></span>
                                        {{$marca['marca']}}
                                        <small>{{$marca->filas}}</small>
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="aside">
                        <div class="size15 m-t-26">
                            <button type="submit" id="filter-btn" class="ship-btn sizefull">
                                <i class="fa fa-filter"></i> Filtrar
                            </button>
                        </div>
                    </div>
                    <!-- /aside Widget -->
                    </form>
                </div>
                <!-- /ASIDE -->

                <!-- STORE -->
                <div id="store" class="col-md-9">
                    <!-- store top filter -->
                    <div class="store-filter clearfix">
                        <div class="store-sort">
                            <label>
                                Ordenar por:
                                <select class="input-select">
                                    <option value="0">Relevancia</option>
                                    <option value="1">Precio: Más barato primero</option>
                                    <option value="2">Precio: Más caro primero</option>
                                    <option value="3">Ofertas</option>
                                </select>
                            </label>
                        </div>
                    </div>
                    <!-- /store top filter -->

                    <!-- store products -->
                    <div id="productos-filter" class="row">
                        @foreach($products as $producto)
                            <!-- product -->
                            <div class="col-md-4 col-xs-6">
                                    <div class="product">
                                        <div class="product-img">
                                            <img src="https://thumb.pccomponentes.com/w-220-220/articles/13/137320/1373200.jpg" alt="">
                                            @if($producto->descuento>0)
                                                <div class="product-label">
                                                    <span class="sale">-{{$producto->descuento}}%</span>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="product-body">
                                            <p class="product-category">
                                                    @foreach($category as $item)
                                                        {{ $item->nombre }}
                                                    @endforeach
                                            </p>
                                        <h3 class="product-name"><a href="/product/{{$producto->id}}">{{ $producto->nombre}}</a></h3>
                                        @if($producto->descuento == 0)
                                            <h4 class="product-price">{{$producto->pvp}}€</h4>
                                        @else
                                            <h4 class="product-price">{{$producto->pvp-($producto->pvp*$producto->descuento/100)}}€ <del class="product-old-price">{{$producto->pvp}}€</del></h4>
                                        @endif
                                            {{-- estrellas --}}
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            {{-- fin estrellas --}}
                                            <div class="product-btns">
                                                <button class="quick-view" onclick="verProducto({{ $producto->id }})"><i class="fa fa-eye"></i><span class="tooltipp">Ver producto</span></button>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <button class="add-to-cart-btn" onclick="anyadirCarrito({{ $producto->id }}, 1)"><i class="fa fa-shopping-cart"></i> Añadir al carrito</button>
                                        </div>
                                    </div>
                                </div>
                            <!-- /product -->
                        @endforeach
                    </div>
                    <!-- /store products -->
                </div>
                <!-- /STORE -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /SECTION -->
@stop