<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>ESta es tu facura</title>
</head>
<body>
<p>Hola! Esta es nu factura de compra en ICEBIRD.</p>
<p>Estos son los productos comprados:</p>
<ul>
    <li>Nombre: {{ $distressCall->user->name }}</li>
    <li>Teléfono: {{ $distressCall->user->phone }}</li>
    <li>DNI: {{ $distressCall->user->dni }}</li>
</ul>
<p>Y esta es la posición reportada:</p>
<ul>
    <li>Latitud: {{ $distressCall->lat }}</li>
    <li>Longitud: {{ $distressCall->lng }}</li>
    <li>
        <a href="https://www.google.com/maps/dir/{{ $distressCall->lat }},{{ $distressCall->lng }}">
            Ver en Google Maps
        </a>
    </li>
</ul>
</body>
</html>