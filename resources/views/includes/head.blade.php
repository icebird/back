<meta charset="utf-8">
<title>Icebird</title>
<link rel="shortcut icon" href="{{{ asset('assets/images/favicon.ico') }}}">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="{{ asset("assets/css/bootstrap.min.css")}}"/>
<link type="text/css" rel="stylesheet" href="{{ asset("assets/css/slick.css")}}"/>
<link type="text/css" rel="stylesheet" href="{{ asset("assets/css/slick-theme.css")}}"/>
<link type="text/css" rel="stylesheet" href="{{ asset("assets/css/nouislider.min.css")}}"/>
<link type="text/css" rel="stylesheet" href="{{ asset("assets/css/style.css")}}"/>
<link type="text/css" rel="stylesheet" href="{{ asset("assets/css/main.css")}}"/>
<link type="text/css" rel="stylesheet" href="{{ asset("assets/css/util.css")}}"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="{{ asset("assets/js/bootstrap.min.js")}}"></script>
<script src="{{ asset("assets/js/slick.min.js")}}"></script>
<script src="{{ asset("assets/js/nouislider.min.js")}}"></script>
<script src="{{ asset("assets/js/jquery.zoom.min.js")}}"></script>
<script src="{{ asset("assets/js/main.js")}}"></script>