<header>
    <div id="top-header">
        <div class="container">
            <ul class="header-links pull-right">
                    @if(Auth::guest())
                    <li><a href="{{ route('login') }}"><i class="fa fa-user-o"></i>
                        Iniciar Sesion
                        </a></li>
                        <li style="color: white">o</li>
                    <li><a href="{{ route('register') }}"><i class="fa fa-user-plus"></i>
                            Registrate
                        </a></li>
                    @else
                        <div class="dropdown">
                            <button id="menu2" type="button" data-toggle="dropdown">
                                Hola, {{ Auth::user()->user }} <span class="caret"></span></button>
                            <ul id="user-drop" class="dropdown-menu" role="menu" aria-labelledby="menu2">
                                <li role="presentation"><i style="margin-left:10px; margin-top:5px" class="fa fa-user-circle-o"></i><a style="float:right" role="menuitem" tabindex="-1" href="/profile">Mi Cuenta</a></li>
                                <li role="presentation" class="divider"></li>
                                <li role="presentation"><i style="margin-left:10px; margin-top:5px" class="fa fa-sign-out"></i><a style="float:right" role="menuitem" tabindex="-1" href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">Cerrar sesión</a><form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                    @endif
            </ul>
        </div>
    </div>
    <div id="header">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="header-logo">
                        <a href="/" class="logo">
                            <img src={{ asset('/assets/images/logo.png') }} width="90px">
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="header-search">
                        <form>
                            <select id="search-select" class="input-select">
                                @foreach(\App\Categoria::all() as $categoria)
                                    <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                                @endforeach
                            </select>
                            <input id="search-input" class="input" placeholder="Ej: God of War">
                            <button type="button" class="search-btn">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                        </form>
                    </div>
                </div>
                <div class="col-md-3 clearfix">
                    <div class="header-ctn">
                        <div class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                <i class="fa fa-shopping-cart"></i>
                                <span>Mi carrito</span>
                                <div id="numcart" class="qty">3</div>
                            </a>
                            <div class="cart-dropdown">
                                <div class="cart-list"></div>
                                <div class="cart-summary">
                                    <small id="count-product-cart"></small>
                                    <h5>TOTAL: <span id="price-product-cart"></span>€</h5>
                                </div>
                                <div class="cart-btns">
                                    <a href="/cart">Ver carrito</a>
                                    <a href='/payment'>Pagar  <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="menu-toggle">
                            <a href="#">
                                <i class="fa fa-bars"></i>
                                <span>Menu</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<nav id="navigation">
    <div class="container">
        <div id="responsive-nav">
            <ul class="main-nav nav navbar-nav">
                <li class="active"><a href="/">Home</a></li>
                <li><a href="#">Ofertas</a></li>
                <li>
                    <button class="nav-drop dropdown-toggle" id="menu1" type="button" data-toggle="dropdown">Categorias
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                        @foreach(\App\Categoria::all() as $categoria)
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="/category/{{$categoria->id}}">{{$categoria->nombre}}</a></li>
                        @endforeach 
                    </ul>
                </li>
                <li><a href="#">Videojuegos</a></li>
                <li><a href="#">Merchandising</a></li>
                <li><a href="#">Mas vendidos</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">¿Qué producto quiere ver?</h4>
        </div>
        <div class="modal-body">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</div>