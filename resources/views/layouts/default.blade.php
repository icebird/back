
<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>
        <div id="myheader">@include('includes.header')</div>
        <div>
                <div id="mycontent">@yield('content')</div>
        </div>
        <div id="myfooter">@include('includes.footer')</div>
</body>
</html>

