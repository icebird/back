//var globales
var url = "http://localhost:8000"
//fin var globales
function num(mynum) {
    var idProductos = []
    for(i=0;i<mynum.length;i++) {
        idProductos.push(mynum[i][0])
    }
    return idProductos
}
function NumProductsCart() {
    var carrito = localStorage.getItem('carrito');
    var num = 0
    if (carrito!=null) {
        var cart=JSON.parse(carrito);
        for (i=0;i<cart.length;i++) {
            num+=cart[i][1]
        }
    }
    return num
}
function anyadirCarrito(id, quant) {
    // localStorage.clear();
	if (quant==-5) {
		quant = parseInt($('#quantity').val())
	}
    var carrito = localStorage.getItem('carrito');
    if (carrito!=null) {
        var cart=JSON.parse(carrito);
        var comp = num(cart).indexOf(id)
        if (comp!=-1) {
            cart[comp][1] = cart[comp][1] + quant
        }
        else {
            var tam = cart.length;
            var aux = [id, quant];
            cart.push(aux);
        }
        localStorage.setItem('carrito', JSON.stringify(cart));
    }
    else {
        var cart=[];
        var aux=[id, quant];
        cart.push(aux);
        localStorage.setItem('carrito', JSON.stringify(cart));
    }
    $('.cart-list').empty()
    $('#count-product-cart').empty()
    $('#price-product-cart').empty()
    productosCarrito()
}

function verProducto(id) {
	location.href = url + '/product/' + id
}
function deleteProduct(id) {
    var carrito = localStorage.getItem('carrito');
    var cart=JSON.parse(carrito);
    for (i=0;i<cart.length;i++) {
        if(cart[i][0]==id) {
            cart[i][1]-=1
            if(cart[i][1]<=0) {
                cart.splice(i,1)
            }
        }
    }
    localStorage.setItem('carrito', JSON.stringify(cart));
    $('.cart-list').empty();
    $('#count-product-cart').empty()
    $('#price-product-cart').empty()
    productosCarrito()
}
function datosProductosCarrito(id, quant) {
    $.ajax({
        type: "get",
        url: url + '/productoCart/' + id,
        success: function (data) {
            $('.cart-list').append(
                '<div class=\"product-widget\">'+
                '<div class="product-img">'+
                '<img src="https://thumb.pccomponentes.com/w-220-220/articles/13/137320/1373200.jpg" width="60px">'+
                '</div>'+
                '<div class="product-body">'+
                '<h3 class="product-name"><a href="#">'+data.data.nombre+'</a></h3>'+
                '<h4 class="product-price"><span class="qty">'+quant+'x</span>'+(((data.data.pvp)-(data.data.pvp)*(data.data.descuento/100))*quant)+'€</h4>'+
                '</div>'+
                '<button onclick="deleteProduct('+data.data.id+')" class="delete"><i class="fa fa-times"></i></button>'+
                '</div>');
        },
        error: function (data) {
            console.log('Error'+ data)
        }
    })
}
function precioTotal() {
    var carrito = localStorage.getItem('carrito');
    if (carrito!=null) {
        $.ajax({
            type: "get",
            url: url + '/PrecioPedido',
            data: {
                'array': carrito,
            },
            success: function (data) {
				$('#price-product-cart').text(data);
				$('#precio_final').text(data+'€');
				$('#price-product-final').text(data+'€')
            },
            error: function (data) {
                console.log('Error'+ data)
            }
        })
    }
}
function productosCarrito(){
    $('#numcart').text(NumProductsCart());
    $('#count-product-cart').text(NumProductsCart()+' productos')
    precioTotal()
    var carrito = localStorage.getItem('carrito');
    var total = 0
    if (carrito!=null) {
        var cart=JSON.parse(carrito);
        for (i=0; i<cart.length;i++) {
            datosProductosCarrito(cart[i][0], cart[i][1])
        }
    }
}
function LlenarProductosCarritoPage(arr){
	$('#cart-page').empty()
	for (var i=0;i<arr.length;i++) {
		$.ajax({
			type: "get",
			url: url + '/productoCarta/' +arr[i][0],
			data: {
				'valor': i,
			},
			success: function (data) {
				if (data.value==0) {
                    $('#cart-page').append(
                    	'<tr class="table-head">'+
						'<th class="column-1"></th>'+
						'<th class="column-2">Nombre del producto</th>'+
						'<th class="column-3">Precio</th>'+
						'<th class="column-4 p-l-70">Cantidad</th>'+
						'<th class="column-5">Total</th>'+
						'</tr>')
				}
				$('#cart-page').append(
					'<tr>'+
					'<td class="column-1">'+
					'<div class="cart-img-product b-rad-4 o-f-hidden">'+
					'<div id="delPro'+data.id+'" class="cont-img">'+
                    '<img style="width:100%;" src="https://thumb.pccomponentes.com/w-220-220/articles/13/137320/1373200.jpg" alt="IMG-PRODUCT">'+
					'<i class="fa fa-times delete-product"></i>'+
                    '</div>'+
					'</div>'+
					'</td>'+
                    '<td class="column-2">'+data.nombre+'</td>'+
					'<td class="column-3">'+((data.pvp)-((data.pvp)*(data.descuento/100)))+'€</td>'+
					'<td class="column-4">'+
					'<div class="flex-w bo5 of-hidden w-size17">'+
					'<button class="btn-num-product-down color1 flex-c-m size7 bg8 eff2">'+
					'<i class="fs-12 fa fa-minus" aria-hidden="true"></i>'+
					'</button>'+
					'<input class="size8 m-text18 t-center num-product" type="number" name="num-product1" value="'+arr[data.value][1]+'">'+
					'<button class="btn-num-product-up color1 flex-c-m size7 bg8 eff2">'+
					'<i class="fs-12 fa fa-plus" aria-hidden="true"></i>'+
					'</button>'+
					'</div>'+
					'</td>'+
					'<td class="column-5">'+(((data.pvp)-(data.pvp)*(data.descuento/100))*arr[data.value][1])+'€</td>'+
					'</tr>'
				);
				precioTotal()
            },
		})
	}
}
function productosCarritoPage() {
    var carrito = localStorage.getItem('carrito');
    if (carrito!=null) {
        var cart=JSON.parse(carrito);
        LlenarProductosCarritoPage(cart)

    }
}
function BotonFiltro(genero, marca, min, max, categoria) {
	console.log('gen: '+genero)
	console.log('marc: '+marca)
	console.log('min: '+min)
	console.log('max: '+max)
	console.log('cat: '+categoria)
	$.ajax({
		type: "get",	
        url: url + '/ProductosFiltros',
        data: {
            'gen': genero,
			'marc': marca,
			'min': min,
			'max': max,
			'cat': categoria,
        },
		success: function (data) {
			for (i=0;i<data.length;i++) {
                $('#productos-filter').empty()
                $('#productos-filter').append(
                    '<div class="col-md-4 col-xs-6">'+
                    '<div class="product">'+
                    '<div class="product-img">'+
                    '<img src="https://thumb.pccomponentes.com/w-220-220/articles/13/137320/1373200.jpg" alt="">')
                if (data.descuento>0) {
                    $('#productos-filter').append(
                        '<div class="product-label">'+
                        '<span class="sale">'+data[i].descuento+'%</span>'+
                        '</div>')
                }
                $('#productos-filter').append(
                    '</div>'+
                    '<div class="product-body">'+
                    '<p class="product-category">'+$('.category-id').val()+'</p>'+
					'<h3 class="product-name"><a href="/product/'+data[i].id+'">'+data[i].nombre+'</a></h3>'
                )
				if (data[i].descuento==0) {
                    $('#productos-filter').append(
                    	'<h4 class="product-price">'+data[i].pvp+'€</h4>'
					)
				} else {
                    $('#productos-filter').append(
                        '<h4 class="product-price">'+data[i].pvp-(data[i].pvp*(data[i].descuento/100))+'€ <del class="product-old-price">'+data[i].pvp+'</del></h4>'
                    )
				}
                $('#productos-filter').append(
                	'<div class="product-rating">'+
					'<i class="fa fa-star"></i>'+
					'<i class="fa fa-star"></i>'+
					'<i class="fa fa-star"></i>'+
					'<i class="fa fa-star"></i>'+
					'<i class="fa fa-star"></i>'+
					'</div>'+
					'<div class="product-btns">'+
					'<button class="quick-view" onclick="verProducto('+data[i].id+'"><i class="fa fa-eye"></i><span class="tooltipp">Ver producto</span></button>'+
					'</div>'+
					'</div>'+
					'<div class="add-to-cart">'+
					'<button class="add-to-cart-btn" onclick="anyadirCarrito('+data[i].id+', 1)"><i class="fa fa-shopping-cart"></i> Añadir al carrito</button>'+
					'</div>'+
					'</div>'+
					'</div>'
				)
			}
        }
	})
}
function generarProvincias(comunidad, provincia) {
	$(comunidad).on('change', function() {
		var id_comunidad = $(comunidad).val()
		$.ajax({
			type: "get",
			url: url + '/provincias/' + id_comunidad,
			success: function(data) {
				$(provincia).empty()
				$(provincia).append('<option value="0">--- SELECCIONAR PROVINCIA ---</option>')
				data.forEach(element => {
					$(provincia).append(
						'<option value="'+ element.id +'">'+ element.provincia +'</option>'
					)
				});
			}
		})
	})
}

function generarMunicipios(provincia, municipio) {
	$(provincia).on('change', function() {
		var id_provincia = $(provincia).val()
		$.ajax({
			type: "get",
			url: url + '/municipios/' + id_provincia,
			success: function(data) {
				$(municipio).empty()
				$(municipio).append('<option value="0">--- SELECCIONAR MUNICIPIO ---</option>')
				data.forEach(element => {
					$(municipio).append(
						'<option value="'+ element.id +'">'+ element.municipio +'</option>'
					)
				});
			}
		})
	})
}
function realizarCompra() {
	var carrito = localStorage.getItem('carrito');
	var cart=JSON.parse(carrito);
	if ($('#terms').prop('checked') && cart!=null) {
		$.ajax({
			type: "get",
			url: url + '/confirmed',
			success: function(data) {
				console.log(data)
				if (data!=0) {
					$.ajax({
						type: "get",
						url: url + '/GuardarPedido',
						data: {
							'datos':carrito
						},
						success: function (data) {
							var f = "no"
							if ($('#shiping-address').is(':checked')) {
								f = "si";
							} 
							$.ajax({
								type: "get",
								url: url + '/hacerPedido',
								data: {
									'nombre': $('#nombre').val(),
									'apellido': $('#apellido').val(),
									'dni': $('#dni').val(),
									'direccion': $('#direccion').val(),
									'id_comunidad_fact': $('[name=1]').val(),
									'id_provincia_fact': $('[name=2]').val(),
									'id_municipio_fact': $('[name=3]').val(),
									'status_envio': f,
									'direc_envio': $('#address_envio').val(),
									'id_comunidad_env': $('[name=4]').val(),
									'id_provincia_env': $('[name=5]').val(),
									'id_municipio_env': $('[name=6]').val(),
								}
							});
							localStorage.clear();
							alert("Producto comprado satisfactoriamente, compruebe su cuenta")
						}
					})
				} else {
					alert("Confirma tu cuenta para poder comprar!");
				}
			}
		})
	} else if(cart==null) {
		alert("Tienes que añadir algún producto!");
	} else {
		alert("Tienes que aceptar los terminos y condiciones!")
	}
}
$(document).ready( function(){
	productosCarritoPage()
	productosCarrito()
	generarProvincias('#comunidad', '#provincia')
	generarMunicipios('#provincia', '#municipio')
	generarProvincias('#comunidad2', '#provincia2')
	generarMunicipios('#provincia2', '#municipio2')

	$('#comunidad').on('change', function() {
		
	})
	$('#dialog').dialog({
		autoOpen: false,
		modal: true,
	});
	$('.search-btn').on('click', function() {
		$('#search-input').css({ 'color': 'black'})
		var categoria = $('#search-select').val()
		var nombre = $('#search-input').val()
		$.ajax({
			type: "get",
			url: url + '/search',
			data: {
				nombre: nombre,
				categoria: categoria,
			},
			success: function(data) {
				if (data.length == 1) {
					window.location.href = url + '/product/' + data[0]
				}
				if (data.length == 0) {
					$('#search-input').css({ 'color': 'red' })
				}
				if (data.length > 1) {
					$('.modal-body').empty()
					$('.modal-body').append(
						'<table id="table-search" class="table"><tr><th>Nombre</th><th>Ver</th></tr><tbody>'
					)
					data.forEach(element => {
						$.ajax({
							type: "get",
							url: url + '/productoCart/' + element,
							success: function(data) {
								console.log(data)
								$('#table-search').append('<tr><td>' + data.data.nombre + '</td><td><a href="'+ url + '/product/' + data.data.id +'"><i class="fa fa-eye"></i></a></td></tr>')
							}
						})
					});
					$('.modal-body').append(
						'</table>'
					)
					$('#myModal').modal('show');
				}
			}
		})
	})

	$('.btn-num-product-down').on('click', function(e){
        e.preventDefault();
        var numProduct = Number($(this).next().val());
        if(numProduct > 1) $(this).next().val(numProduct - 1);
    });

    $('.btn-num-product-up').on('click', function(e){
        e.preventDefault();
        var numProduct = Number($(this).prev().val());
        $(this).prev().val(numProduct + 1);
    });

	$('.review-link').on('click', function(){
		$('#first-tab').removeClass('active');
		$('#second-tab').addClass('active');
	})

	$('.gender').each(function(){
		var text = $(this).text();
		var text2=text.substring(text.indexOf('"')+1,text.lastIndexOf('"'));
		$(this).text(text2);
	});

	$('.input-number').each(function() {
		var $this = $(this),
		$input = $this.find('input[type="number"]'),
		up = $this.find('.qty-up'),
		down = $this.find('.qty-down');
	
		down.on('click', function () {
			var value = parseInt($input.val()) - 1;
			value = value < 1 ? 1 : value;
			$input.val(value);
			$input.change();
			updatePriceSlider($this , value)
		})
	
		up.on('click', function () {
			var value = parseInt($input.val()) + 1;
			$input.val(value);
			$input.change();
			updatePriceSlider($this , value)
		})
	});

	$('.menu-toggle > a').on('click', function (e) {
	e.preventDefault();
	$('#responsive-nav').toggleClass('active');

})

// Fix cart dropdown from closing
$('.cart-dropdown').on('click', function (e) {
	e.stopPropagation();
});

/////////////////////////////////////////

// Products Slick
$('.products-slick').each(function() {
	var $this = $(this),
			$nav = $this.attr('data-nav');

	$this.slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		infinite: true,
		speed: 300,
		dots: false,
		arrows: true,
		appendArrows: $nav ? $nav : false,
		responsive: [{
				breakpoint: 991,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			},
		]
	});
});

// Products Widget Slick
$('.products-widget-slick').each(function() {
	var $this = $(this),
			$nav = $this.attr('data-nav');

	$this.slick({
		infinite: true,
		autoplay: true,
		speed: 300,
		dots: false,
		arrows: true,
		appendArrows: $nav ? $nav : false,
	});
});

/////////////////////////////////////////

// Product Main img Slick
$('#product-main-img').slick({
	infinite: true,
	speed: 300,
	dots: false,
	arrows: true,
	fade: true,
	asNavFor: '#product-imgs',
});

// Product imgs Slick
$('#product-imgs').slick({
	slidesToShow: 3,
	slidesToScroll: 1,
	arrows: true,
	centerMode: true,
	focusOnSelect: true,
	centerPadding: 0,
	vertical: true,
	asNavFor: '#product-main-img',
	responsive: [{
			breakpoint: 991,
			settings: {
				vertical: false,
				arrows: false,
				dots: true,
			}
		},
	]
});

// Product img zoom
var zoomMainProduct = document.getElementById('product-main-img');
if (zoomMainProduct) {
	$('#product-main-img .product-preview').zoom();
}

/////////////////////////////////////////

// Input number


var priceInputMax = document.getElementById('price-max'),
		priceInputMin = document.getElementById('price-min');

priceInputMax.addEventListener('change', function(){
	updatePriceSlider($(this).parent() , this.value)
});

priceInputMin.addEventListener('change', function(){
	updatePriceSlider($(this).parent() , this.value)
});

function updatePriceSlider(elem , value) {
	if ( elem.hasClass('price-min') ) {
		console.log('min')
		priceSlider.noUiSlider.set([value, null]);
	} else if ( elem.hasClass('price-max')) {
		console.log('max')
		priceSlider.noUiSlider.set([null, value]);
	}
}

// Price Slider
var priceSlider = document.getElementById('price-slider');
if (priceSlider) {
	noUiSlider.create(priceSlider, {
		start: [1, 500],
		connect: true,
		step: 1,
		range: {
			'min': 1,
			'max': 500,
		}
	});

	priceSlider.noUiSlider.on('update', function( values, handle ) {
		var value = values[handle];
		handle ? priceInputMax.value = value : priceInputMin.value = value
	});
}
})
