/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Instalación de Laravel Passport

Ejecutaremos dentro dewl proyecto el siguiente comando para configurar Passport.

~~~bash
composer require laravel/passport
~~~

Ahora abrimos el fichero *config/app.php* y añadimos la siguietne línea en el array de *providers*.

~~~bash
Laravel\Passport\PassportServiceProvider::class,
~~~
Modificamos el archivo *.env* y añadimos los datos de nuestra base de datos.
Ahora crearemos las migraciones que necesitaremos y añadiremos los campos que queramos en la tabla *User*, en mi caso he creado las siguientes migraciones: 
- Categorias
- Clientes
- Comentarios
- Generos
- Pedidos
- Productos
- Productos_reservados
- Reservas
- User

A continuación ejecutaremos:
~~~bash
php artisan migrate
~~~
Ahora tendremos todas las tablas creadas y las tablas necesarias para el funcionamiento de Passport.
El siguiente paso es ejecutar el siguiente comando para generar las claves de cifrado necesarias para generar tokens de acceso seguro.
~~~bash
php artisan passport:install
~~~


### Listar rutas
```
php artisan route:list
```


## Configuración de Laravel Passport
En el modelo de User añadiremos estas lineas para usar HasApiTokens:
~~~php
use Laravel\Passport\HasApiTokens;
class User extends Authenticatable
{
    use HasApiTokens,Notifiable;
~~~
Ahora modificamos el archivo app/Providers/AuthServiceProvider.php
~~~php
use Laravel\Passport\Passport; boot()
public function boot()
{

    $this->registerPolicies();
    Passport::routes();
}
~~~
Para acabar cambiamos el driver de autentificación `config/auth.php`
~~~php
'guards' => [
    'web' => [
        'driver' => 'session',
        'provider' => 'users',
    ],

    'api' => [
        'driver' => 'passport',//Aqui añadimos Passport
        'provider' => 'users',
    ],
],

~~~
## Autentificándonos con Laravel Passport
Crearemos un controlador para procesar el login y el registro de un usuario, para ello ejecutamos lo siguiente:
~~~bash
php artisan make:controller Api/AuthController
~~~
Abrimos el controlador y añadimos las siguientes lineas en la parte superior:
~~~php
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
~~~

## CORS (Alexander)

<https://gist.github.com/technoknol/1a35ca4b150215f491d5c807940bd4ef>

