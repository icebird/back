<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_usuario')->references('id')->on('users');
            $table->string('nombre');
            $table->string('user-game')->nullable();
            $table->string('dni', 15);
            $table->text('ProductosComprados');
            $table->string('direccion_envio');
            $table->string('direccion_facturacion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clentes');
    }
}
