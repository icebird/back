<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnyadirCamposAProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('stock');
            $table->float('pvp',8,2);
            $table->integer('descuento')->nullable();
            $table->float('precio',8,2);
            $table->integer('idGenero')->references('id')->on('generos');
            $table->integer('idCategoria')->references('id')->on('categorias');
            $table->integer('valoracion')->nullable();
            $table->text('descripcion');
            $table->string('marca');
            $table->string('distribuidor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
