<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoReservadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_reservados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idUsuario')->references('id')->on('users');
            $table->integer('idReserva')->references('id')->on('reservas');
            $table->integer('idProducto')->references('id')->on('productos');
            $table->string('estado')->default('En espera');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_reservados');
    }
}
